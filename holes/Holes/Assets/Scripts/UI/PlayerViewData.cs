﻿using System;
using System.Collections.Generic;

public class PlayerViewData
{
    public readonly WeaponViewData[] weapons;

    public PlayerViewData(params WeaponViewData[] weapons)
    {
        this.weapons = weapons ?? throw new ArgumentNullException(nameof(weapons));
    }
}