﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    [SerializeField] private WeaponView weaponViewPrefab;
    [SerializeField] private Transform weaponsViewParent;

    public void Show(PlayerViewData playerViewData)
    {
        Clear();
        foreach(WeaponViewData weapon in playerViewData.weapons)
        {
            Instantiate(weaponViewPrefab, weaponsViewParent).Show(weapon);
        }
    }

    private void Clear()
    {
        for (int i = weaponsViewParent.childCount; i-- > 0;)
        {
            Destroy(weaponsViewParent.GetChild(i).gameObject);
        }
    }
}
