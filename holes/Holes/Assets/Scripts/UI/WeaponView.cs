﻿using System;
using UnityEngine;
using UnityEngine.UI;

internal class WeaponView : MonoBehaviour
{
    [SerializeField] private Image image;
    [SerializeField] private Color inactive;
    [SerializeField] private Color active;

    internal void Show(WeaponViewData weapon)
    {
        image.sprite = weapon.image;
        image.color = weapon.isActive ? active : inactive;
    }
}