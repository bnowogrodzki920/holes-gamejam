﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class GameplayUI : MonoBehaviour
{
    public PlayerDeadView playerDeadView;
    public WinningView winningView;
    public PlayerUI playerView;
    public TutorialUI tutorialUI;
}
