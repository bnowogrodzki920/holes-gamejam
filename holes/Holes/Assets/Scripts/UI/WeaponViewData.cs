﻿using System;
using UnityEngine;

public class WeaponViewData
{
    public readonly Sprite image;
    public readonly bool isActive;

    public WeaponViewData(Sprite image, bool isActive)
    {
        this.image = image ?? throw new ArgumentNullException(nameof(image));
        this.isActive = isActive;
    }
}