﻿using Assets.Scripts.Obstacles;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour, IPlayer
{
    private GameController gameController;
    [SerializeField] private float fallSpeed = 5f;
    [SerializeField] private float followSpeed = 3f;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private Gun gun;

    public List<IPlayerListener> listeners = new List<IPlayerListener>();

    private Vector3 destinationPoint;
    private List<GunData> guns = new List<GunData>();
    int currentGun = 0;

    private void Start()
    {
        destinationPoint = transform.position;
    }

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
    }

    private void Update()
    {
        Follow();
        Shoot();
        SwitchGuns();
    }

    private void Follow()
    {
        if (gameController.InputController.inputs.leftPressed)
        {
            destinationPoint = gameController.MainCamera.ScreenToWorldPoint(new Vector3(gameController.InputController.inputs.mousePos.x,
                                                                                        gameController.InputController.inputs.mousePos.y, -10f));
        }
        Vector2 dir = (destinationPoint - transform.position).normalized;
        rb.velocity = new Vector2(dir.x * followSpeed, dir.y * followSpeed);
    }

    private void Shoot()
    {
        if (gameController.InputController.inputs.rightPressed && guns.Count > 0)
        {
            gun.Shoot();
        }
    }

    private void SwitchGuns()
    {
        if (gameController.InputController.inputs.scrollUp && currentGun > 0)
        {
            gun.SetGunData(guns[--currentGun]);
        }
        else if (gameController.InputController.inputs.scrollDown && currentGun < guns.Count - 1)
        {
            gun.SetGunData(guns[++currentGun]);
        }
        if(guns.Count > 0)
        {
            gameController.GameplayUI.playerView.Show(GeneratePlayerView());
        }
    }

    private PlayerViewData GeneratePlayerView()
    {
        WeaponViewData[] weapons = new WeaponViewData[guns.Count];
        for (int i = 0; i < guns.Count; i++)
        {
            weapons[i] = new WeaponViewData(guns[i].Image, i == currentGun);
        }
        return new PlayerViewData(weapons);
    }

    public void Die()
    {
        listeners.ForEach((l) => l.OnPlayerDied());
        Destroy(this);
    }

    public void CatchGun(GunData gunData)
    {
        guns.Add(gunData);
        gun.SetGunData(gunData);
        currentGun = guns.Count - 1;
        gameController.GameplayUI.playerView.Show(GeneratePlayerView());
        listeners.ForEach((l) => l.OnGunCaught());
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag(Config.Tags.POINT_TAG))
        {
            gameController.ProgressController.AddPoint();
            Destroy(other.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag(Config.Tags.POINT_TAG))
        {
            gameController.ProgressController.AddPoint();
            Destroy(collision.gameObject);
        }
    }
}
