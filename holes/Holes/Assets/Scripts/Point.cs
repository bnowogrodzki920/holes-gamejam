﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class Point : MonoBehaviour
{
    [SerializeField] private float upSpeed;

    private void Update()
    {
        transform.Translate(new Vector3(0, upSpeed * Time.deltaTime, 0));
    }
}
