﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

class Background : MonoBehaviour
{
    [SerializeField] Transform original;
    [SerializeField] Transform second;
    [SerializeField] private SpriteRenderer[] originalImages;
    [SerializeField] private SpriteRenderer[] secondImages;
    public float movementVelocity;

    private Vector3 spawnPlace;
    private Vector3 repeatPlace;

    private Queue<Sprite> spriteSwapQueue = new Queue<Sprite>();
    private Sprite currentSprite;

    private void Start()
    {
        spawnPlace = second.position;
        repeatPlace = original.position;
    }

    private void Update()
    {
        original.Translate(new Vector3(0, movementVelocity * Time.deltaTime, 0));
        second.Translate(new Vector3(0, movementVelocity * Time.deltaTime, 0));
        if(second.transform.position.y > repeatPlace.y)
        {
            original.transform.position = spawnPlace + new Vector3(0, second.transform.position.y - repeatPlace.y, 0);
            if(spriteSwapQueue.Count > 0)
            {
                currentSprite = spriteSwapQueue.Dequeue();
            }
            if (currentSprite)
            {
                foreach (SpriteRenderer image in originalImages)
                {
                    image.sprite = currentSprite;
                }
            }

            Transform swap = original;
            original = second;
            second = swap;

            SpriteRenderer[] imageSwap = originalImages;
            originalImages = secondImages;
            secondImages = imageSwap;
        }
    }

    public void SetImages(Sprite sprite)
    {
        spriteSwapQueue.Enqueue(sprite);
    }
}
