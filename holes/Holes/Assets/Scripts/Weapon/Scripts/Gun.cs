﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class Gun : MonoBehaviour
{
    [SerializeField] private Transform bulletsSpawnPoint;
    [SerializeField] private Transform bulletParent;
    [SerializeField] private Transform explosionsParent;
    [SerializeField] private SpriteRenderer gunSprite;
    [SerializeField] private GunData gunData;

    private float lastShotTime;

    public void SetGunData(GunData gunData)
    {
        this.gunData = gunData;
        this.gunSprite.sprite = gunData.Image;
        lastShotTime = -gunData.CooldownTime;
    }

    public float CooldownTime => gunData.CooldownTime;
    public bool canShoot => Time.time - lastShotTime > gunData.CooldownTime;

    private void Start()
    {
        if (gunData)
        {
            lastShotTime = -gunData.CooldownTime;
        }
    }

    public void Shoot()
    {
        if (canShoot)
        {
            ShotIgnoreCooldown();
            lastShotTime = Time.time;
        }
    }

    public void ShotIgnoreCooldown()
    {
        Bullet bullet = Instantiate(gunData.BulletPrefab, bulletsSpawnPoint.position, Quaternion.identity, bulletParent);
        bullet.Init(explosionsParent);
    }
}
