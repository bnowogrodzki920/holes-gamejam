﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[CreateAssetMenu(fileName = "GunData", menuName = "Guns/Gun")]
public class GunData : ScriptableObject
{
    [SerializeField] private Sprite image;
    [SerializeField] private Bullet bulletPrefab;
    [SerializeField] private float cooldownTime;

    public Bullet BulletPrefab => bulletPrefab;
    public float CooldownTime => cooldownTime;
    public Sprite Image => image;
}
