﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class Explosion : MonoBehaviour
{
    [SerializeField] private int strength;
    [SerializeField] private ObstacleType efectiveOn;
    [SerializeField] private AnimationCurve strengthFalloff;
    [SerializeField] new private CircleCollider2D collider;

    private void Start()
    {
        StartCoroutine(DestroyingCoroutine());
    }

    public ObstacleType EfectivenessType => efectiveOn;

    public float ExplosionForceAt(Vector3 position)
    {
        float distance = (transform.position - position).magnitude;
        float multiplier = strengthFalloff.Evaluate((collider.radius - distance) / collider.radius);
        return strength * multiplier;
    }

    private IEnumerator DestroyingCoroutine()
    {
        yield return new WaitForFixedUpdate();
        Destroy(this);
        Destroy(gameObject.GetComponent<Collider2D>());
    }
}
