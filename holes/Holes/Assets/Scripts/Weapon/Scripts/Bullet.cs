﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float speed = 5;
    [SerializeField] private GameObject explosionPrefab;
    [SerializeField] private Transform explosionsParent;

    public void Init(Transform explosionsParent)
    {
        this.explosionsParent = explosionsParent;
    }

    private void Update()
    {
        transform.Translate(new Vector3(0, -speed * Time.deltaTime, 0));
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == Config.Tags.OBSTACLE_TAG)
        {
            Instantiate(explosionPrefab, transform.position, Quaternion.identity, explosionsParent);
            Destroy(this.gameObject);
        }
    }
}
