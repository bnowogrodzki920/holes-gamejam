﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class PickableGun : MonoBehaviour
{
    [SerializeField] private GunData gun;
    [SerializeField] private float velocity;

    public GunData Gun => gun;

    private void Update()
    {
        transform.Translate(new Vector3(0, velocity, 0) * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == Config.Tags.PLAYER_TAG)
        {
            collision.gameObject.GetComponent<PlayerController>().CatchGun(gun);
            Destroy(this.gameObject);
        }
    }
}
