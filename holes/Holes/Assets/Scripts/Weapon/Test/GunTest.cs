﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Weapon.Test
{
    class GunTest : MonoBehaviour
    {
        [SerializeField] private Gun gun;
        [SerializeField] private ObstacleGeneratorConfiguration obstacleConfig;
        [SerializeField] private ObstacleRandomInstancer obstacleInstancer;

        private bool shoot = false;

        private void Update()
        {
            if (shoot)
            {
                gun.Shoot();
            }
        }

        [Button]
        private void GenerateRandomObstacle()
        {
            obstacleConfig.Validate();
            obstacleConfig.randomSeed = new System.Random().Next();
            obstacleInstancer.SetRandomSeed(obstacleConfig.randomSeed);
            obstacleInstancer.InstantiateObstacle(new ObstacleGenerator(obstacleConfig).GenerateNextObstacle(), new Vector2(0, 0));
        }

        [Button]
        private void ShootGun()
        {
            gun.ShotIgnoreCooldown();
        }

        [Button]
        private void EnableShooting()
        {
            this.shoot = true;
        }

        [Button]
        private void DisableShooting()
        {
            this.shoot = false;
        }

        private void OnValidate()
        {
            obstacleConfig.Validate();
        }
    }
}
