﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public class ProgressController : MonoBehaviour
{
    [SerializeField] private ProgressData progressData;

    public void AddPoint()
    {
        AddPoints(1);
    }
    public void AddPoints(int points)
    {
        progressData.SetCurrentScore(progressData.CurrentScore + points);
        if(progressData.CurrentScore > progressData.Highscore)
        {
            progressData.SetHighScore(progressData.CurrentScore);
        }
    }
}
