﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class TutorialController : MonoBehaviour, IPlayerListener
{
    [SerializeField] private float steeringTutorialStartTime;

    private GameController gameController;
    private float countingTimeStart;
    private int gunsCount = 0;

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
        this.countingTimeStart = Time.time;
        gameController.Player.listeners.Add(this);
    }

    private void Update()
    {
        if(Time.time > steeringTutorialStartTime + countingTimeStart)
        {
            gameController.SetState(new TutorialState(SteeringTutorialFinished, 0.7f, 0.1f, "Steer by holding LMB, and moving mouse."));
            steeringTutorialStartTime = float.MaxValue;
        }
    }

    private bool SteeringTutorialFinished()
    {
        return gameController.InputController.inputs.leftPressed;
    }

    public void OnPlayerDied()
    {

    }

    public void OnGunCaught()
    {
        gunsCount++;
        if(gunsCount == 1)
        {
            gameController.SetState(new TutorialState(ShootingTutorialFinished, 0.7f, 0.1f, "You can shoot by holding RMB."));
        }
        if (gunsCount == 2)
        {
            gameController.SetState(new TutorialState(SwitchingWeaponTutorialFinished, 0.7f, 0.1f, "You can change weapon with mouse scroll."));
        }
    }

    private bool ShootingTutorialFinished()
    {
        return gameController.InputController.inputs.rightPressed;
    }
    private bool SwitchingWeaponTutorialFinished()
    {
        return gameController.InputController.inputs.scrollUp;
    }
}
