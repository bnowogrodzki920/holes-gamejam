﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class GameplayState : IGameState, IPlayerListener, ICourseControllerListener
{
    GameController gameController;

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
        gameController.Player.listeners.Add(this);
        gameController.CourseController.listener = this;
    }

    public void StartState()
    {
        Time.timeScale = 1;
    }

    public void UpdateState()
    {

    }

    public void EndState()
    {
        Time.timeScale = 0;
    }

    public void OnPlayerDied()
    {
        gameController.SetState(new PlayerDeadState());
    }

    public void OnCourseFinished()
    {
        gameController.SetState(new GameFinishedState());
    }

    public void OnGunCaught()
    {

    }
}
