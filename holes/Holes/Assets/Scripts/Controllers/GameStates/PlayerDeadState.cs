﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class PlayerDeadState : IGameState
{
    private GameController gameController;

    public void EndState()
    {

    }

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
    }

    public void StartState()
    {
        gameController.GameplayUI.playerDeadView.Show();
    }

    public void UpdateState()
    {

    }
}
