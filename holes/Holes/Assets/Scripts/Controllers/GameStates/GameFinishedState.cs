﻿internal class GameFinishedState : IGameState
{
    private GameController gameController;

    public void EndState()
    {

    }

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
    }

    public void StartState()
    {
        gameController.GameplayUI.winningView.Show();
    }

    public void UpdateState()
    {

    }
}