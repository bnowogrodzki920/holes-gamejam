﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class TutorialState : IGameState, IPlayerListener
{
    private GameController gameController;
    private Func<bool> tutorialFinishedFunction;
    private float slowDownTime;
    private float slowDownStartTime;
    private float slowTimeValue;
    private bool finished;
    private string tutorialText;

    public TutorialState(Func<bool> tutorialFinishedFunction, float slowDownTime, float slowTimeValue, string tutorialText)
    {
        this.tutorialFinishedFunction = tutorialFinishedFunction ?? throw new ArgumentNullException(nameof(tutorialFinishedFunction));
        this.slowDownTime = slowDownTime;
        this.slowTimeValue = slowTimeValue;
        this.tutorialText = tutorialText ?? throw new ArgumentNullException(nameof(tutorialText));
    }

    public void EndState()
    {
        Time.timeScale = 1;
        gameController.GameplayUI.tutorialUI.Hide();
    }

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
    }

    public void OnGunCaught()
    {

    }

    public void OnPlayerDied()
    {
        gameController.SetState(new PlayerDeadState());
    }

    public void StartState()
    {
        slowDownStartTime = Time.time;
        Time.timeScale = 1;
        gameController.GameplayUI.tutorialUI.Show(tutorialText);
    }

    public void UpdateState()
    {
        if (!finished)
        {
            if(Time.time - slowDownStartTime < slowDownTime)
            {
                Time.timeScale = Mathfx.Sinerp(1, slowTimeValue, (Time.time - slowDownStartTime) / slowDownTime);
            }
            if (tutorialFinishedFunction.Invoke())
            {
                finished = true;
                slowDownStartTime = Time.time;
                gameController.GameplayUI.tutorialUI.Hide();
            }
        }
        else
        {
            Time.timeScale = Mathfx.Sinerp(slowTimeValue, 1, (Time.time - slowDownStartTime) / slowDownTime);
            if(Time.timeScale > 0.95f)
            {
                gameController.SetState(new GameplayState());
            }
        }
    }
}
