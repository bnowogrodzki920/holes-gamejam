﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public interface IGameState
{
    void Init(GameController gameController);
    void StartState();
    void UpdateState();
    void EndState();
}
