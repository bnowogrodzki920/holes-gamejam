﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [SerializeField] private Camera mainCamera;
    public Camera MainCamera { get { return mainCamera; } }

    [SerializeField] private PlayerController player;
    public PlayerController Player { get { return player; } }

    [SerializeField] private InputController inputController;
    public InputController InputController { get { return inputController; } }

    [SerializeField] private CourseController courseController;
    public CourseController CourseController { get { return courseController; } }

    [SerializeField] private TutorialController tutorialController;
    public TutorialController TutorialController { get { return tutorialController; } }

    [SerializeField] private GameplayUI gameplayUI;
    public GameplayUI GameplayUI { get { return gameplayUI; } }

    [SerializeField] private ProgressController progressController;
    public ProgressController ProgressController { get { return progressController; } }

    private IGameState currentState;

    private void Start()
    {
        player.Init(this);
        courseController.Init(this);
        tutorialController.Init(this);
        SetState(new GameplayState());
    }

    public void SetState(IGameState state)
    {
        currentState?.EndState();
        currentState = state;
        currentState.Init(this);
        currentState.StartState();
    }

    private void Update()
    {
        currentState?.UpdateState();
    }
}
