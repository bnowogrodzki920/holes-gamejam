﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private Transform followedTransform;
    [SerializeField] private float followSpeed;

    private void Update()
    {
        Follow();
    }

    private void Follow()
    {
        this.transform.position = SmoothPath(this.transform.position, followedTransform.position);
    }

    private Vector3 SmoothPath(Vector3 from, Vector3 to)
    {
        return new Vector3(Mathf.SmoothStep(from.x, to.x, followSpeed * Time.deltaTime),
            Mathf.SmoothStep(from.y, to.y, followSpeed * Time.deltaTime), this.transform.position.z);
    }
}
