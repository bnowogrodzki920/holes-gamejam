﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

public class CourseController : MonoBehaviour
{
    [SerializeField] private List<CourseConfigEntry> configs;
    [SerializeField] private ObstacleRandomInstancer instancer;
    [SerializeField] private float firstWeaponSpawnTime;
    [SerializeField] private PickableGun firstWeapon;
    [SerializeField] private Transform firstWeaponSpawnPoint;
    [SerializeField] private BackgroundController backgroundController;

    private GameController gameController;

    private CourseConfigEntry currentConfig;
    private ObstacleGenerator generator;
    [ShowNonSerializedField] private int currentStage;
    private float stageStartTime;
    private float lastObstacleGenerationTime;

    public ICourseControllerListener listener;

    private bool shouldEnableNextStage => Time.time - stageStartTime > currentConfig.time + currentConfig.delayTime;
    private bool shouldGenerateNextObstacle => Time.time - (lastObstacleGenerationTime + currentConfig.delayTime) > 1/currentConfig.obstaclesGeneratingFrequency;

    public void Init(GameController gameController)
    {
        this.gameController = gameController;
    }

    private void Start()
    {
        MoveToStage(0);
        lastObstacleGenerationTime = Time.time;
        System.Random random = new System.Random();
        foreach(CourseConfigEntry config in configs)
        {
            config.obstacleGeneratingConfig.randomSeed = random.Next();
        }
    }

    private void Update()
    {
        if (shouldEnableNextStage)
        {
            if (currentStage + 1 < configs.Count)
            {
                MoveToStage(currentStage + 1);
            }
            else
            {
                listener?.OnCourseFinished();
            }
        }
        if(firstWeaponSpawnTime > 0)
        {
            firstWeaponSpawnTime -= Time.deltaTime;
            if(firstWeaponSpawnTime <= 0)
            {
                Instantiate(firstWeapon, firstWeaponSpawnPoint.position, Quaternion.identity);
            }
        }
        if (shouldGenerateNextObstacle)
        {
            instancer.InstantiateObstacle(generator.GenerateNextObstacle(), currentConfig.minMaxObstacleVelocity);
            lastObstacleGenerationTime = Time.time;
        }
    }

    private void MoveToStage(int index)
    {
        currentConfig = configs[index];
        currentStage = index;
        stageStartTime = Time.time;
        generator = new ObstacleGenerator(currentConfig.obstacleGeneratingConfig);
        backgroundController.SetBackgrounds(currentConfig.backgroundConfig);
    }

    private void OnValidate()
    {
        foreach(CourseConfigEntry entry in configs)
        {
            entry.obstacleGeneratingConfig.Validate();
        }
    }

    [Serializable]
    private class CourseConfigEntry
    {
        public ObstacleGeneratorConfiguration obstacleGeneratingConfig;
        public BackgroundConfig backgroundConfig;
        public float obstaclesGeneratingFrequency;
        public Vector2 minMaxObstacleVelocity;
        public float time;
        public float delayTime = 0;
    }

    [Serializable]
    public class BackgroundConfig
    {
        public Sprite backgroundImage;
        public Sprite sidesImage;
        public Sprite frontsImage;
    }
}
