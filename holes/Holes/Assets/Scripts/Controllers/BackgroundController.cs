﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class BackgroundController : MonoBehaviour
{
    [SerializeField] private Background background;
    [SerializeField] private Background sideImages;
    [SerializeField] private Background frontImages;

    public void SetBackgrounds(CourseController.BackgroundConfig config)
    {
        background.SetImages(config.backgroundImage);
        sideImages.SetImages(config.sidesImage);
        frontImages.SetImages(config.frontsImage);
    }
}
