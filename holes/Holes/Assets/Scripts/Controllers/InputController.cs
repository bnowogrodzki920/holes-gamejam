﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    public InputValues inputs = new InputValues();
    private float startTimeOfLeftButtonClicked;
    private int clickCount = 0;

    public struct InputValues
    {
        public float xMouse;
        public float yMouse;

        public float xMousePos;
        public float yMousePos;
        public Vector3 mousePos;

        public bool leftClicked;
        public bool leftPressed;
        public bool rightClicked;
        public bool rightPressed;

        public float scroll;
        internal bool scrollUp;
        internal bool scrollDown;
    }

    public void Update()
    {
        inputs.xMouse = Input.GetAxisRaw("Mouse X");
        inputs.yMouse = Input.GetAxisRaw("Mouse Y");
        inputs.scrollUp = Input.GetAxisRaw("Mouse ScrollWheel") > 0;
        inputs.scrollDown = Input.GetAxisRaw("Mouse ScrollWheel") < 0;
            
        inputs.xMousePos = Input.mousePosition.x;
        inputs.yMousePos = Input.mousePosition.y;
        inputs.mousePos = Input.mousePosition;

        inputs.leftClicked = Input.GetMouseButtonDown(0);
        inputs.leftPressed = Input.GetMouseButton(0);
        inputs.rightClicked = Input.GetMouseButtonDown(1);
        inputs.rightPressed = Input.GetMouseButton(1);
    }
}
