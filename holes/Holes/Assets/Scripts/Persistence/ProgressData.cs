﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "ProgressData", menuName = "ProgressData")]
public class ProgressData : ScriptableObject
{
    public int Highscore { get; private set; }
    public int CurrentScore { get; private set; }

    public void SetCurrentScore(int count)
    {
        CurrentScore = count;
    }

    public void SetHighScore(int highscore)
    {
        Highscore = highscore;
    }

    public void Clear()
    {
        Highscore = 0;
        CurrentScore = 0;
    }

}
