﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

class ObstacleAreaGeneratorTest : MonoBehaviour
{
    [SerializeField] private ObstacleGeneratorAreaConfig config;
    [SerializeField] private int seed;
    ObstacleArea generated;

    [Button]
    private void RandomTest()
    {
        seed = new System.Random().Next();
        Test();
#if UNITY_EDITOR
        EditorWindow view = EditorWindow.GetWindow<SceneView>();
        view.Repaint();
#endif
    }

    [Button]
    private void Test()
    {
        AreaGenerator areaGenerator = new AreaGenerator(config, seed);
        generated = areaGenerator.GenerateNextArea();
    }
    private void OnDrawGizmosSelected()
    {
        if (generated != null)
        {
            IEnumerator<Vector2Int> positions = generated.GetEnumerator();
            while (positions.MoveNext())
            {
                Gizmos.DrawCube(new Vector3(positions.Current.x, positions.Current.y, 0), Vector3.one);
            }
        }
    }
}
