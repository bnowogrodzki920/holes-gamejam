﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

class ObstacleGeneratorTestClass : MonoBehaviour
{
    [SerializeField] private ObstacleGeneratorConfiguration config = null;
    [SerializeField] private Gradient tilesColorsDiversity = null;

    private ObstacleGenerator generator;
    private ObstacleTileData[,] generated;

    

    [Button]
    public void RandomTest()
    {
        NextRandomSeed();
        generated = generator.GenerateNextObstacle();
#if UNITY_EDITOR
        EditorWindow view = EditorWindow.GetWindow<SceneView>();
        view.Repaint();
#endif
    }
    
    [Button]
    public void RepeatTest()
    {
        generator = new ObstacleGenerator(config);
        generated = generator.GenerateNextObstacle();
#if UNITY_EDITOR
        EditorWindow view = EditorWindow.GetWindow<SceneView>();
        view.Repaint();
#endif
    }

    [Button]
    public void NextRandomSeed()
    {
        generated = null;
        config.randomSeed = new System.Random().Next();
        OnValidate();
    }

    private void OnDrawGizmosSelected()
    {
        if (generated != null)
        {
            for(int x = 0; x < generated.GetLength(0); x++)
            {
                for (int y = 0; y < generated.GetLength(1); y++)
                {
                    if(generated[x,y] != null)
                    {
                        Gizmos.color = tilesColorsDiversity.Evaluate(CheckColor(generated[x, y]));
                        Gizmos.DrawCube(new Vector3(x, y, 0), Vector3.one);
                    }
                }
            }
        }
    }

    float CheckColor(ObstacleTileData obstacleTileData)
    {
        int i = 0;
        for(; i < config.tilesConfig.tilesSet.Length; i++)
        {
            if(obstacleTileData == config.tilesConfig.tilesSet[i])
            {
                break;
            }
        }
        return (float)i / (config.tilesConfig.tilesSet.Length - 1);
    }

    private void OnValidate()
    {
        config.Validate();
        generator = new ObstacleGenerator(config);
        generated = null;
    }
}
