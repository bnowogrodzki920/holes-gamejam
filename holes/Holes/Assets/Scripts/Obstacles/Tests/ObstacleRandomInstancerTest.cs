﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class ObstacleRandomInstancerTest : MonoBehaviour
{
    [SerializeField] private ObstacleGeneratorConfiguration config;
    [SerializeField] private ObstacleRandomInstancer instancer;
    [SerializeField] private bool clearBeforeTest;
    [SerializeField] private Transform obstaclesParent;
    [SerializeField] private Vector2 minMaxVelocity;

    [Button]
    private void RandomTest()
    {
        config.randomSeed = new System.Random().Next();
        Test();
    }

    [Button]
    private void RepeatTest()
    {
        Test();
    }

    private void Test()
    {
        if (clearBeforeTest)
        {
            for (int i = 0; i < obstaclesParent.childCount; i++)
            {
                DestroyImmediate(obstaclesParent.GetChild(i).gameObject);
            }
        }
        ObstacleGenerator generator = new ObstacleGenerator(config);
        instancer.SetRandomSeed(config.randomSeed);
        instancer.InstantiateObstacle(generator.GenerateNextObstacle(), minMaxVelocity);
    }

    private void OnValidate()
    {
        config.Validate();
    }
}
