﻿using Assets.Scripts.Obstacles;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ObstacleTile : MonoBehaviour
{
    [SerializeField] private ParticleSystem destroyParticleSystem = null;
    [SerializeField] private GameObject pointPrefab = null;

    private ObstacleTileData data;

    public void Init(ObstacleTileData data)
    {
        this.data = data;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.tag)
        {
            case Config.Tags.EXPLOSION_TAG:
                OnExplosionHit(collision.gameObject.GetComponent<Explosion>());
                break;
            case Config.Tags.PLAYER_TAG:
                OnPlayerHit(collision.GetComponent<IPlayer>());
                break;
        }
    }

    private void OnExplosionHit(Explosion explosion)
    {
        if(explosion.EfectivenessType == data.ObstacleType)
        {
            if (explosion.ExplosionForceAt(transform.position) >= data.Strength)
            {
                if (destroyParticleSystem)
                {
                    Instantiate(destroyParticleSystem, transform.position, Quaternion.identity);
                }
                if (pointPrefab)
                {
                    Instantiate(pointPrefab, transform.position, Quaternion.identity);
                }
                OnDestroyed();
                Destroy(this.gameObject);
            }
        }
    }

    private void OnPlayerHit(IPlayer player)
    {
        player?.Die();
        if (destroyParticleSystem)
        {
            Instantiate(destroyParticleSystem, transform.position, Quaternion.identity);
        }
        Destroy(this.gameObject);
    }

    protected virtual void OnDestroyed() { }
}
