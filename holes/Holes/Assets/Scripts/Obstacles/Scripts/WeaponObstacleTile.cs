﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class WeaponObstacleTile : ObstacleTile
{
    [SerializeField] private PickableGun pickableGunPrefab;

    protected override void OnDestroyed()
    {
        Instantiate(pickableGunPrefab, transform.position, Quaternion.identity);
    }
}
