﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class ObstacleRandomInstancer : MonoBehaviour
{
    [SerializeField] private Transform obstaclesParent;
    [SerializeField] private Transform obstaclesZeroPoint;
    [SerializeField] private Obstacle obstaclePrefab;
    [SerializeField] private int tunnelWidth;
    [SerializeField] private int randomSeed;

    public void InstantiateObstacle(ObstacleTileData[,] obstacle, Vector2 minMaxVelocity)
    {
        Obstacle newObstacle = Instantiate(obstaclePrefab, obstaclesZeroPoint.position, Quaternion.identity, obstaclesParent);
        int x = 0, maxY = 0;
        bool connectedWithWallRight = true, connectedWithWallLeft = true;
        for(; x < obstacle.GetLength(0);x++)
        {
            int y = 0;
            int tilesInColumn = 0;
            for (; y < obstacle.GetLength(1); y++)
            {
                if(obstacle[x,y] != null)
                {
                    ObstacleTile tile = Instantiate(obstacle[x, y].TilePrefab, obstaclesZeroPoint.position + new Vector3(x, y, 0), Quaternion.identity, newObstacle.transform).GetComponent<ObstacleTile>();
                    ObstacleTileData data = obstacle[x, y];
                    tile.Init(data);
                    tilesInColumn++;
                }
            }
            if (y > maxY)
            {
                maxY = y;
            }
            if (tilesInColumn == 1)
            {
                if(x == 0)
                {
                    connectedWithWallLeft = false;
                }
                else if(x == obstacle.GetLength(0) - 1)
                {
                    connectedWithWallRight = false;
                }
            }
        }
        System.Random random = new System.Random(randomSeed);
        float randomFloat = (float)random.NextDouble();
        float randomVelocity = minMaxVelocity.x + ((minMaxVelocity.y - minMaxVelocity.x) * randomFloat);
        newObstacle.Init(randomVelocity, x, maxY);
        int translation;
        int maxOffset = tunnelWidth - newObstacle.sizeX;
        if (connectedWithWallLeft) 
        {
            translation = 0;
        }
        else if (connectedWithWallRight)
        {
            translation = maxOffset;
        }
        else
        {
            translation = random.Next(maxOffset + 1);
        }
        newObstacle.transform.Translate(new Vector3(translation, 0, 0));
    }

    internal void SetRandomSeed(int randomSeed)
    {
        this.randomSeed = randomSeed;
    }
}
