﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class Obstacle : MonoBehaviour
{
    [SerializeField] private float velocity = 0;

    public int sizeX { get; private set; }
    public int sizeY { get; private set; }

    public void Init(float velocity, int sizeX, int sizeY)
    {
        this.velocity = velocity;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }

    public void SetUpwardVelocity(float velocity)
    {
        this.velocity = velocity;
    }

    private void Update()
    {
        transform.Translate(Vector3.up * velocity * Time.deltaTime, Space.World);
    }
}
