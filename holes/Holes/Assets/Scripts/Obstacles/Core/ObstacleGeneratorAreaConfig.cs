﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
class ObstacleGeneratorAreaConfig
{
    [SerializeField] public int tunnelWidth = 5;
    [SerializeField] public int maxWidth = 5;
    [SerializeField] public int minWidth = 2;
    [SerializeField] public int maxHeight = -1;
    [SerializeField] public int minHeight = 1;
    [SerializeField] public int minThickness = 1;
    [SerializeField] public int maxThickness = 3;
    public void Validate()
    {
        if(tunnelWidth < 1)
        {
            throw new Exception("tunnel width must be greater than 0");
        }
        if (maxWidth > 0 && minWidth > maxWidth)
        {
            throw new Exception("minimum width must be less or equal to maximum width");
        }
        if (maxWidth > tunnelWidth)
        {
            throw new Exception("max width must be less or equal to tunnel width");
        }
        if (minHeight > maxHeight)
        {
            throw new Exception("minimum height must be less or equal to maximum height");
        }
        if(minThickness < 1)
        {
            throw new Exception("minimum thickness must be greater than 0");
        }
        if (minHeight < 1)
        {
            throw new Exception("minimum height must be greater than 0");
        }
    }
}
