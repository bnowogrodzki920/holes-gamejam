﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
class ObstacleGeneratorTilesConfig
{
    [SerializeField] public ObstacleTileData[] tilesSet;
    [SerializeField] public int[] tilesMaxCount;
    [SerializeField] public int[] tilesMinCount;
    [SerializeField] public int[] tilesVariety;

    public void Validate()
    {
        if (tilesSet.Length < 1)
        {
            throw new Exception("tiles set must have at least one element!");
        }
        if (tilesSet.Length != tilesMaxCount.Length || tilesSet.Length != tilesMinCount.Length || tilesSet.Length != tilesVariety.Length)
        {
            throw new Exception("tiles set, tiles max count, tiles min count and tiles variety must all have the same elements count!");
        }
        for (int i = 0; i < tilesSet.Length; i++)
        {
            if (tilesMaxCount[i] > 0 &&tilesMinCount[i] > tilesMaxCount[i])
            {
                throw new Exception(string.Format("tiles min count cannot be greater than tiles max count (element: {0})", i));
            }
            if (tilesVariety[i] < 1)
            {
                throw new Exception(string.Format("tiles variety must be at least 1 (element: {0})", i));
            }
        }
    }
}
