﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class ObstacleGenerator
{
    private ObstacleGeneratorConfiguration config;
    private System.Random random;
    private AreaGenerator areaGenerator;

    public ObstacleGenerator(ObstacleGeneratorConfiguration config)
    {
        this.config = config ?? throw new ArgumentNullException(nameof(config));
        random = new System.Random(config.randomSeed);
        areaGenerator = new AreaGenerator(config.areaConfig, config.randomSeed);
    }

    public ObstacleTileData[,] GenerateNextObstacle()
    {
        ObstacleArea area = areaGenerator.GenerateNextArea();
        VarietySeed[] varietySeeds = GenerateVarietySeeds(area, out int[] tilesCount);
        return FillObstacleArea(area, varietySeeds, tilesCount);
    }

    private VarietySeed[] GenerateVarietySeeds(ObstacleArea area, out int[] tilesCount)
    {
        ObstacleGeneratorTilesConfig tilesConfig = config.tilesConfig;
        tilesCount = new int[tilesConfig.tilesSet.Length];
        HashSet<Vector2Int> freeTiles = new HashSet<Vector2Int>();
        int totalSeedsCount = 0;
        for (int i = 0; i < tilesConfig.tilesSet.Length; i++)
        {
            int numberOfSeeds;
            if (tilesConfig.tilesMinCount[i] < tilesConfig.tilesVariety[i])
            {
                numberOfSeeds = random.Next(tilesConfig.tilesMinCount[i], tilesConfig.tilesVariety[i] + 1);
            }
            else
            {
                numberOfSeeds = random.Next(tilesConfig.tilesVariety[i], tilesConfig.tilesMinCount[i] + 1);
            }
            tilesCount[i] = numberOfSeeds;
            totalSeedsCount += numberOfSeeds;
        }
        VarietySeed[] varietySeeds = new VarietySeed[totalSeedsCount];
        int seedIndex = 0;
        for (int tileI =0; tileI < tilesConfig.tilesSet.Length; tileI++)
        {
            for(int seedI =0; seedI < tilesCount[tileI]; seedI++)
            {
                bool done = false;
                while (!done)
                {
                    int x = random.Next(0, area.sizeX), y = random.Next(0, area.sizeY);
                    Vector2Int randomCoordinates = new Vector2Int(x, y);
                    if(area.IsFilled(randomCoordinates.x, randomCoordinates.y) && !IsAny(varietySeeds, randomCoordinates))
                    {
                        varietySeeds[seedIndex] = new VarietySeed(randomCoordinates, tilesConfig.tilesSet[tileI]);
                        seedIndex++;
                        done = true;
                    }
                }

            }
        }
        return varietySeeds;
    }

    private bool IsAny(VarietySeed[] seeds, Vector2Int position)
    {
        for(int i = 0; i<seeds.Length; i++)
        {
            if(seeds[i].position == position)
            {
                return true;
            }
        }
        return false;
    }

    private ObstacleTileData[,] FillObstacleArea(ObstacleArea area, VarietySeed[] varietySeeds, int[] tilesCount)
    {
        ObstacleGeneratorTilesConfig tilesConfig = this.config.tilesConfig;
        ObstacleTileData[,] obstacle = new ObstacleTileData[area.sizeX, area.sizeY];
        List<int> availableTiles = new List<int>();
        for(int i = 0; i < varietySeeds.Length; i++)
        {
            VarietySeed seed = varietySeeds[i];
            obstacle[seed.position.x, seed.position.y] = seed.tileData;
        }
        for(int i = 0; i< tilesConfig.tilesSet.Length; i++)
        {
            if(tilesConfig.tilesMaxCount[i] <= 0 || tilesCount[i] < tilesConfig.tilesMaxCount[i])
            {
                availableTiles.Add(i);
            }
        }
        for (int x = 0; x < area.sizeX; x++)
        {
            for (int y = 0; y < area.sizeY; y++)
            {
                if (area.IsFilled(x, y) && !obstacle[x,y])
                {
                    int tileNumber = availableTiles[random.Next(0, availableTiles.Count)];
                    obstacle[x, y] = tilesConfig.tilesSet[tileNumber];
                    tilesCount[tileNumber] = tilesCount[tileNumber] + 1;
                    if (tilesConfig.tilesMaxCount[tileNumber] > 0 && tilesCount[tileNumber] >= tilesConfig.tilesMaxCount[tileNumber])
                    {
                        availableTiles.Remove(tileNumber);
                    }
                }
            }
        }
        return obstacle;
    }

    private struct VarietySeed
    {
        public readonly Vector2Int position;
        public readonly ObstacleTileData tileData;

        public VarietySeed(Vector2Int position, ObstacleTileData tileData)
        {
            this.position = position;
            this.tileData = tileData ?? throw new ArgumentNullException(nameof(tileData));
        }
    }
}
