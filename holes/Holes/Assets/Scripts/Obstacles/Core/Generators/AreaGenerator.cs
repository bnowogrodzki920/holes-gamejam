﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


class AreaGenerator
{
    private ObstacleGeneratorAreaConfig config;
    private Random random;

    public AreaGenerator(ObstacleGeneratorAreaConfig config, int randomSeed)
    {
        this.config = config ?? throw new ArgumentNullException(nameof(config));
        this.random = new Random(randomSeed);
    }

    public ObstacleArea GenerateNextArea()
    {
        config.Validate();
        ObstacleArea area = GenerateObstacleArea();
        return area;
    }



    private ObstacleArea GenerateObstacleArea()
    {
        byte xSize = (byte)random.Next(config.minWidth, config.maxWidth > 0 ? config.maxWidth + 1 : config.tunnelWidth + 1),
            ySize = (byte)random.Next(config.minHeight, config.maxHeight + 1);
        ObstacleArea obstacleArea = new ObstacleArea(xSize);

        byte[] height = new byte[xSize];
        random.NextBytes(height);
        ContraintHeights(height, ySize);

        bool leftEdgeTouching = xSize == config.tunnelWidth ? true : RandomBool(),
            rightEdgeTouching = leftEdgeTouching ? xSize == config.tunnelWidth : RandomBool();

        height[0] = leftEdgeTouching ? ySize : (byte)1;
        height[xSize - 1] = rightEdgeTouching ? ySize : (byte)1;

        byte offset = (byte)random.Next(0, ySize - height[0] + 1);
        obstacleArea.SetBlock(0, height[0], offset);
        byte previousOffset = offset;

        for (int x = 1; x < xSize; x++)
        {
            int minOffset = Math.Max(previousOffset - (height[x] - 1), 0);
            int maxOffset = Math.Min(previousOffset + height[x - 1] - 1, ySize - height[x]);
            offset = (byte)random.Next(minOffset, maxOffset + 1);
            obstacleArea.SetBlock(x, height[x], offset);
            previousOffset = offset;
        }
        return obstacleArea;
    }

    private void ContraintHeights(byte[] heights, byte obstacleHeight)
    {
        byte maxHeight = (byte)Math.Min(config.maxThickness, obstacleHeight);
        for (int i = 0; i < heights.Length; i++)
        {

            byte newValue = (byte)((heights[i] * maxHeight) / 255);
            heights[i] = newValue >= (byte)config.minThickness ? newValue : (byte)1;
        }
    }

    private bool RandomBool()
    {
        return random.Next(0, 2) == 0;
    }
}
