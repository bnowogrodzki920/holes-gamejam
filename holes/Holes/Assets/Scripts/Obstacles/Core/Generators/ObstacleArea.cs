﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

class ObstacleArea : IEnumerable<Vector2Int>
{
    public readonly int sizeX = 0;

    private int var_sizeY = 0;
    private byte[] heights;
    private byte[] offsets;

    public int sizeY => var_sizeY;

    public ObstacleArea(int sizeX)
    {
        this.sizeX = sizeX;
        heights = new byte[sizeX];
        offsets = new byte[sizeX];
    }

    public void SetBlock(int x, byte height, byte offset)
    {
        heights[x] = height;
        offsets[x] = offset;
        if(height + offset > sizeY)
        {
            var_sizeY = height + offset;
        }
    }

    public byte GetHeight(int x)
    {
        return heights[x];
    }

    public byte GetOffset(int x)
    {
        return offsets[x];
    }

    public bool IsFilled(int x, int y)
    {
        return y< heights[x] + offsets[x] && y >= offsets[x];
    }

    public IEnumerator<Vector2Int> GetEnumerator()
    {
        return new ObstacleAreaEnumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return new ObstacleAreaEnumerator(this);
    }

    public class ObstacleAreaEnumerator : IEnumerator<Vector2Int>
    {
        ObstacleArea area;
        private int x = 0, y = 0;

        public Vector2Int Current => new Vector2Int(x, y);

        object IEnumerator.Current => new Vector2Int(x, y);

        public ObstacleAreaEnumerator(ObstacleArea area)
        {
            this.area = area ?? throw new ArgumentNullException(nameof(area));
            Reset();
        }

        public void Dispose()
        {

        }

        public bool MoveNext()
        {
            if(y < area.sizeY && area.IsFilled(x, y + 1))
            {
                y++;
                return true;
            }
            else if(x < area.sizeX - 1)
            {
                x++;
                y = area.offsets[x];
                return true;
            }
            return false;
        }

        public void Reset()
        {
            x = 0;
            y = area.offsets[x] - 1;
        }
    }
}
