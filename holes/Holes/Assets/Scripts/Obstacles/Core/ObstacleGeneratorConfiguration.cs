﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

[Serializable]
class ObstacleGeneratorConfiguration
{
    [SerializeField] public ObstacleGeneratorAreaConfig areaConfig;
    [SerializeField] public int randomSeed = 5;
    [SerializeField] public ObstacleGeneratorTilesConfig tilesConfig;

    public void Validate()
    {
        areaConfig.Validate();
        tilesConfig.Validate();
    }
}
