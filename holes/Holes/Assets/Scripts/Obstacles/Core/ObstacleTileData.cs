﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


[CreateAssetMenu(fileName = "SolidObstacleTile", menuName = "Obstacles/ObstacleTile")]
public class ObstacleTileData : ScriptableObject
{
    [SerializeField] private ObstacleType obstacleType;
    [SerializeField] private GameObject tilePrefab = null;
    [SerializeField] private float strength = 1;

    public GameObject TilePrefab => tilePrefab;
    public float Strength => strength;

    public ObstacleType ObstacleType => obstacleType;
}
