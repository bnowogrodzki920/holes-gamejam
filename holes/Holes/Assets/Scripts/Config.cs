﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


static class Config
{
    public static class Tags
    {
        public const string EXPLOSION_TAG = "Explosion";
        public const string PLAYER_TAG = "Player";
        public const string OBSTACLE_TAG = "Obstacle";
        public const string POINT_TAG = "Point";
    }
}
